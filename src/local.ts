import { LocalDexie } from "./stores/local.dexie";

const appName = "Pointron";
const defaultAppMenu: string[] = ["journal", "focus", "goal", "analytics"];

const defaultAppData = {
  name: appName,
  version: "0.0.1",
  leftPanelFooter: "simple",
  homePath: "focus",
  cp: {
    // modules: ["home", "goals", "insights"],
    customization: [
      "theme",
      "session",
      "analytics",
      "presets",
      "targets",
      "alerts",
      // "appMenu",
      // "widgets",
      "shortcuts"
    ],
    app: ["accessibility", "data", "feedback", "about"]
  }
};

export { LocalDexie, defaultAppData, defaultAppMenu };
