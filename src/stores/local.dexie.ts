import { AppDexie } from "$lib/client/persistence/dexie";

export class LocalDexie extends AppDexie {
  constructor(scope: string) {
    super(scope);
  }
}
