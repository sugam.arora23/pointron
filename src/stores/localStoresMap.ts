import { analyticsConfigStore } from "$lib/client/products/pointron/analytics/analytics.store";
import {
  focusItemsStore,
  sessionStore
} from "$lib/client/products/pointron/focus/session.store";
import {
  goalStore,
  newGoalStore,
  quickFocusItemStore
} from "$lib/client/products/pointron/goals/goal.store";
import { focusHeatmapStore } from "$lib/client/products/pointron/journal/journal.store";

import {
  logsPaneStore,
  pointLogStore
} from "$lib/client/products/pointron/logs/log.store";
import {
  pointronPreferences,
  tagStore
} from "$lib/client/products/pointron/pointron.store";
import type { CacheableStoreContract } from "$lib/client/types/data.type";

export const localCacheableStores: CacheableStoreContract[] = [
  goalStore,
  tagStore,
  quickFocusItemStore,
  pointronPreferences,
  focusItemsStore,
  sessionStore,
  newGoalStore,
  pointLogStore,
  logsPaneStore,
  focusHeatmapStore,
  analyticsConfigStore
];
