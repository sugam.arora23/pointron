import type { Preview, StoryFn } from "@storybook/svelte";
import DocumentationTemplate from "./DocumentationTemplate.mdx";
import "../src/app.css";
import "../src/lib/tidy/components/storybook/preview.css";
import ThemeWrapper from "../src/lib/tidy/components/storybook/ThemeWrapper.svelte";
const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i
      }
    },
    backgrounds: {
      values: [
        { name: "red", value: "#f00" },
        { name: "green", value: "#0f0" },
        { name: "blue", value: "#00f" }
      ]
    },
    docs: {
      page: DocumentationTemplate,
      toc: {
        contentsSelector: ".sbdocs-content",
        headingSelector: "h1, h2, h3",
        ignoreSelector: "#primary",
        title: "Table of Contents",
        disable: false,
        unsafeTocbotOptions: {
          orderedList: false
        }
      }
    }
    // viewport: {
    //   viewports: {
    //     desktop: {
    //       name: "Desktop",
    //       styles: {
    //         width: "1280px",
    //         height: "800px"
    //       }
    //     },
    //     mobile: {
    //       name: "Mobile",
    //       styles: {
    //         width: "375px",
    //         height: "667px"
    //       }
    //     }
    //   },
    //   defaultViewport: "desktop"
    // }
  },
  args: {},
  decorators: [
    () => ({
      Component: ThemeWrapper,
      props: {}
    })
  ]
};

export default preview;
